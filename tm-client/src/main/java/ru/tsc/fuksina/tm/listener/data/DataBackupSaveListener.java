package ru.tsc.fuksina.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.DataBackupSaveRequest;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.event.ConsoleEvent;

@Component
public final class DataBackupSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "backup-save";

    @NotNull
    public static final String DESCRIPTION = "Save backup in base64 file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBackupSaveListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        getDomainEndpoint().saveDataBackup(new DataBackupSaveRequest(getToken()));
    }

}
