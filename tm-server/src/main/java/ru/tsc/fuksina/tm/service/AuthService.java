package ru.tsc.fuksina.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.fuksina.tm.api.service.IAuthService;
import ru.tsc.fuksina.tm.api.service.IPropertyService;
import ru.tsc.fuksina.tm.api.service.dto.IUserDtoService;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.exception.field.LoginEmptyException;
import ru.tsc.fuksina.tm.exception.field.PasswordEmptyException;
import ru.tsc.fuksina.tm.exception.system.AccessDeniedException;
import ru.tsc.fuksina.tm.exception.system.AuthenticationException;
import ru.tsc.fuksina.tm.dto.model.SessionDto;
import ru.tsc.fuksina.tm.dto.model.UserDto;
import ru.tsc.fuksina.tm.util.CryptUtil;
import ru.tsc.fuksina.tm.util.HashUtil;

import java.util.Date;
import java.util.Optional;

@Service
public class AuthService implements IAuthService {

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    public UserDto registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDto validateToken(@Nullable final String token) {
        Optional.ofNullable(token).orElseThrow(AccessDeniedException::new);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (Exception e ) {
            throw new AccessDeniedException(e);
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull SessionDto session = objectMapper.readValue(json, SessionDto.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        @Nullable final UserDto user = userService.findOneByLogin(login);
        Optional.ofNullable(user).orElseThrow(AuthenticationException::new);
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        Optional.ofNullable(hash).orElseThrow(AuthenticationException::new);
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return getToken(user);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDto user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDto session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDto createSession(@NotNull final UserDto user) {
        @NotNull final SessionDto session = new SessionDto();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return session;
    }

}
