package ru.tsc.fuksina.tm.service.dto;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.fuksina.tm.api.service.dto.ITaskDtoService;
import ru.tsc.fuksina.tm.dto.model.TaskDto;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.exception.field.*;
import ru.tsc.fuksina.tm.repository.dto.TaskDtoRepository;

import java.util.*;

@Getter
@Service
public class TaskDtoService extends AbstractUserOwnedServiceDto<TaskDto, TaskDtoRepository> implements ITaskDtoService {

    @NotNull
    @Autowired
    private TaskDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public TaskDto create(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        @NotNull final TaskDto task = new TaskDto();
        task.setUserId(userId);
        task.setName(name);
        @NotNull final TaskDtoRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final TaskDto task = new TaskDto();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final TaskDtoRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateStart,
            @Nullable final Date dateEnd
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final TaskDto task = new TaskDto();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateEnd(dateEnd);
        @NotNull final TaskDtoRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDto updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final TaskDto task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        @NotNull final TaskDtoRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDto changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);
        @NotNull final TaskDto task = findOneById(userId, id);
        task.setStatus(status);
        @NotNull final TaskDtoRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDto> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final TaskDtoRepository repository = getRepository();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

}
