package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void writeLog(@Nullable String message);

}
