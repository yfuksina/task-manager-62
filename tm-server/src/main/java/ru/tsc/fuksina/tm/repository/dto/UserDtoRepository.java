package ru.tsc.fuksina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.dto.model.UserDto;

@Repository
public interface UserDtoRepository extends AbstractDtoRepository<UserDto> {

    @Nullable
    UserDto findFirstByLogin(@NotNull final String login);

    @Nullable
    UserDto findFirstByEmail(@NotNull final String email);

}
